<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Обработчик</title>
    <link href="style.css" rel="stylesheet">
    <link href="red-button.css" rel="stylesheet">
</head>
<body>
    <?php
        require_once 'conn.php';
        $stm = 
"INSERT INTO lesson(week_day, lesson_number, auditorium, disciple, type) 
    VALUES(:day, :les_num, :aud, :sbj, 'Practical')";
        $pdo_stm = $pdo->prepare($stm);
        $res = $pdo_stm->execute(
            array(':day' => $_POST['day'], ':les_num' => $_POST['les_num'],
                 ':aud' => $_POST['aud'], 'sbj' => $_POST['sbj'])
        );
        
        $les_id = $pdo->query("SELECT LAST_INSERT_ID()")->fetch()['LAST_INSERT_ID()']; 
        
        $stm =
"INSERT INTO lesson_teacher(FID_Teacher, FID_Lesson1)
    VALUES(:tch_id, :les_id)";
        $pdo_stm = $pdo->prepare($stm);
        $pdo_stm->execute(
            array(':tch_id' => $_POST['tch_id'],
                 ':les_id' => $les_id)
        );

        $stm =
"INSERT INTO lesson_groups(FID_Lesson2, FID_Groups)
    VALUES(:les_id, :gr_id)";
        $pdo_stm = $pdo->prepare($stm);
        $pdo_stm->execute(
            array(':les_id' => $les_id,
                 ':gr_id' => $_POST['gr_id'])
        );
    ?>
    <h1>Запрос должен был быть выполнен успешно.</h1>
    <a href="." class="like-red-button">На главную страницу</a>
</body>
</html>