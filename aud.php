<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Teacher Shedule</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <a href="." style="display: block; text-align: start;">&ltНа главную страницу</a>
    <h1>
        Расписание аудитории
        <?php
            echo $_GET['aud'];
        ?>
    </h1>
    <table border="1">
        <tr>
            <th>Преподаватель</th>
            <th>День недели</th>
            <th>Пара</th>
            <th>Аудитория</th>
            <th>Предмет</th>
            <th>Тип занятия</th>
            <th>Группы</th>
        </tr>
            
        <?php
            require_once 'conn.php';

            $stm = 
"SELECT t.name, l.* FROM lesson AS l
    INNER JOIN lesson_teacher AS lt ON lt.fid_lesson1=l.id_lesson
    INNER JOIN teacher AS t ON t.id_teacher=lt.fid_teacher
    WHERE l.auditorium LIKE :aud";
            
            $pdo_stm = $pdo->prepare($stm);
            $pdo_stm->execute(
                array(':aud' => $_GET['aud'])
            );
            $pdo_stm->setFetchMode(PDO::FETCH_ASSOC);

            $stm2 = 
"SELECT g.title FROM lesson AS l 
    INNER JOIN lesson_groups AS lg ON lg.fid_lesson2=l.id_lesson
    INNER JOIN groups AS g ON g.id_groups=lg.fid_groups
    WHERE l.ID_Lesson=:les_id;";
            $pdo_stm2 = $pdo->prepare($stm2);
            $pdo_stm2->setFetchMode(PDO::FETCH_ASSOC);

            foreach ($pdo_stm as $row) {
                echo '<tr>';
                foreach ($row as $key => $value) {
                    if ($key == "ID_Lesson"){
                        continue;
                    }
                    echo '<td>' . $value . '</td>';
                }
                                
                $pdo_stm2->execute(
                    array(':les_id' => $row['ID_Lesson'])
                );

                echo '<td>';
                foreach ($pdo_stm2 as $group) {
                    echo $group['title'] . ' ';
                }
                echo '</td>';
                
                echo '</tr>';
            }
            
        ?>
    </table>
</body>
</html>