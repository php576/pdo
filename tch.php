<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Teacher Shedule</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <a href="." style="display: block; text-align: start;">&ltНа главную страницу</a>
    <h1>
        Расписание преподавателя
        <?php
            require_once 'conn.php';
            $stm = 
"SELECT name FROM teacher
    WHERE ID_Teacher=:tch_id";
            
            $pdo_stm = $pdo->prepare($stm);
            $pdo_stm->execute(
                array(':tch_id' => $_GET['tch_id'])
            );
            echo $pdo_stm->fetch()['name'];
        ?>
    </h1>
    <table border="1">
        <tr>
            <th>День недели</th>
            <th>Пара</th>
            <th>Аудитория</th>
            <th>Предмет</th>
            <th>Тип занятия</th>
            <th>Группы</th>
        </tr>
            
        <?php
            $stm = 
"SELECT l.* FROM teacher AS t
    INNER JOIN lesson_teacher AS lt ON t.id_teacher=lt.fid_teacher
    INNER JOIN lesson AS l ON l.id_lesson=lt.fid_lesson1
    WHERE t.id_teacher=:tch_id;";
            
            $pdo_stm = $pdo->prepare($stm);
            $pdo_stm->execute(
                array(':tch_id' => $_GET['tch_id'])
            );
            $pdo_stm->setFetchMode(PDO::FETCH_ASSOC);

            $stm2 = 
"SELECT g.title FROM lesson AS l 
    INNER JOIN lesson_groups AS lg ON lg.fid_lesson2=l.id_lesson
    INNER JOIN groups AS g ON g.id_groups=lg.fid_groups
    WHERE l.ID_Lesson=:les_id;";
            $pdo_stm2 = $pdo->prepare($stm2);
            $pdo_stm2->setFetchMode(PDO::FETCH_ASSOC);

            foreach ($pdo_stm as $row) {
                echo '<tr>';
                foreach ($row as $key => $value) {
                    if ($key == "ID_Lesson"){
                        continue;
                    }
                    echo '<td>' . $value . '</td>';
                }
                                
                $pdo_stm2->execute(
                    array(':les_id' => $row['ID_Lesson'])
                );

                echo '<td>';
                foreach ($pdo_stm2 as $group) {
                    echo $group['title'] . ' ';
                }
                echo '</td>';
                
                echo '</tr>';
            }
            
        ?>
    </table>
</body>
</html>