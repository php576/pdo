<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Shcedule</title>
    <link href="style.css" rel="stylesheet">
    <link href="red-button.css" rel="stylesheet">
</head>
<body>
   <h1>Расписание</h1>
    <form action="group.php">
        <label for="groups">Выберите группу:</label>
    <?php  
    require_once 'conn.php';
    include 'gr_select.php';        
    ?>
         <input type="submit" value="Показать">
    </form>

    <form action="tch.php">
        Выберите преподавателя:
        <?php
            include 'tch_select.php';
        ?>
        <input type="submit" value="Показать">
    </form>
    
    <form action="aud.php">
        <label for="auditoriums">
            Выберите аудиторию:
        </label>
        <select id="auditoriums" name="aud">
            <?php
                $stm = 
"SELECT DISTINCT auditorium FROM lesson";
                foreach ($pdo->query($stm) as $row) {
                    $aud = $row['auditorium'];
                    echo "<option value=\"$aud\">"
                        . $aud . "</option>";
                }
            ?>
        </select>
        <input type="submit" value="Показать">
    </form>
    <br>
    
    <fieldset style="display: inline-block">
    <legend>Добавить практическое занятие</legend>
    <form id="login_form" method="post" action="handler.php">
	    Преподаватель:
        <?php
            include 'tch_select.php';
        ?>
        <br><br>
        <label for="f2">Дисциплина:</label>
        <input type="text" value="Computer Networks" id="f2" name="sbj">
        <br><br>
        <label for="f3">День недели:</label>
        <select id="f3" name="day">
            <option value="Monday">Monday</option>
            <option value="Tuesday">Tuesday</option>
            <option value="Wednesday">Wednesday</option>
            <option value="Thursday">Thursday</option>
            <option value="Friday">Friday</option>
            <option value="Saturday">Saturday</option>
            <option value="Sunday">Sunday</option>
        </select>
        <br>
        <label for="f4">Пара по счёту:</label>
        <select name="les_num" id="f4">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
        </select>
        <br><br>
        <label for="f5">Аудитория:</label>
        <input type="text" name="aud" value="338i" id="f5">
        <br><br>
        <label for="groups">Группа:</label>        
        <?php
            include 'gr_select.php';
        ?>
        <br><br>
	    <input type="submit" value="Добавить" class="like-red-button">
    </form>
	</fieldset>
</body>
</html>