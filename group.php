<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Teacher Shedule</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
   <a href="." style="display: block; text-align: start;">&ltНа главную страницу</a>
    <h1>
        Расписание группы
        <?php
            require_once 'conn.php';
            $stm = 
"SELECT title FROM groups
    WHERE ID_Groups=:gr_id";
            $prep_stm = $pdo->prepare($stm); 
            $prep_stm->execute(
                array(':gr_id' => $_GET['gr_id'])
            );
            $gr_name = $prep_stm->fetch()['title'];
            echo $gr_name;
        ?>
    </h1>
    <table border="1">
        <tr>
            <th>День недели</th>
            <th>Пара</th>
            <th>Аудитория</th>
            <th>Предмет</th>
            <th>Тип занятия</th>
            <th>Преподаватель</th>
        </tr>
        <?php        
            $stm = 
"SELECT l.*, t.name FROM lesson_groups AS lg
    INNER JOIN lesson AS l ON l.ID_Lesson=lg.FID_Lesson2
    INNER JOIN lesson_teacher AS lt ON lt.fid_lesson1=l.id_lesson
    INNER JOIN teacher AS t ON t.id_teacher=lt.fid_teacher
    WHERE lg.fid_groups=:gr_id;";
            
            $pdo_stm = $pdo->prepare($stm);
            $pdo_stm->execute(
                array(':gr_id' => $_GET['gr_id'])
            );
            $pdo_stm->setFetchMode(PDO::FETCH_ASSOC);
            foreach ($pdo_stm as $row) {
                echo '<tr>';
                foreach ($row as $key => $value) {
                    if ($key == "ID_Lesson"){
                        continue;
                    }
                    echo '<td>' . $value . '</td>';
                }
                echo '</tr>';
            }
        ?>
    </table>
</body>
</html>